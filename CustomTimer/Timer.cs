﻿using System;
using CustomTimer.EventArguments;

namespace CustomTimer
{
    /// <summary>
    /// A custom class for simulating a countdown clock, which implements the ability to send a messages and additional
    /// information about the Started, Tick and Stopped events to any types that are subscribing the specified events.
    /// 
    /// - When creating a CustomTimer object, it must be assigned:
    ///     - name (not null or empty string, otherwise ArgumentException will be thrown);
    ///     - the number of ticks (the number must be greater than 0 otherwise an exception will throw an ArgumentException).
    /// 
    /// - After the timer has been created, it should fire the Started event, the event should contain information about
    /// the name of the timer and the number of ticks to start.
    /// 
    /// - After starting the timer, it fires Tick events, which contain information about the name of the timer and
    /// the number of ticks left for triggering, there should be delays between Tick events, delays are modeled by Thread.Sleep.
    /// 
    /// - After all Tick events are triggered, the timer should start the Stopped event, the event should contain information about
    /// the name of the timer.
    /// </summary>
    public class Timer
    {
        private readonly string name;

        private int ticksLeft;

        public Timer(string timerName, int ticks)
        {
            if (string.IsNullOrEmpty(timerName))
            {
                throw new ArgumentException($"{nameof(timerName)} cannot be null or empty.");
            }

            if (ticks <= 0)
            {
                throw new ArgumentException($"{nameof(ticks)} should be greater than zero.");
            }

            this.name = timerName;
            this.ticksLeft = ticks;
        }

        public event EventHandler<NameAndTicksArguments> Started;

        public event EventHandler<NameArgument> Stoped;

        public event EventHandler<NameAndTicksArguments> Tick;

        public void Run()
        {
            this.Started?.Invoke(this, new NameAndTicksArguments() { Name = this.name, Ticks = this.ticksLeft });

            while (this.ticksLeft > 0)
            {
                this.ticksLeft--;
                this.Tick?.Invoke(this, new NameAndTicksArguments() { Name = this.name, Ticks = this.ticksLeft });

                System.Threading.Thread.Sleep(10);
            }

            this.Stoped?.Invoke(this, new NameArgument() { Name = this.name });
        }
    }
}

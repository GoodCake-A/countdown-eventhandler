﻿using System;

namespace CustomTimer.EventArguments
{
    public class NameAndTicksArguments : EventArgs
    {
        public string Name { get; set; }

        public int Ticks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomTimer.EventArguments
{
    public class NameArgument : EventArgs
    {
        public string Name { get; set; }
    }
}

﻿using System;
using CustomTimer.Interfaces;
using CustomTimer.EventArguments;

namespace CustomTimer.Implementation
{
    /// <inheritdoc/>
    public class CountDownNotifier : ICountDownNotifier
    {
        private readonly Timer timer;

        public CountDownNotifier(Timer timer)
        {
            this.timer = timer ?? throw new ArgumentNullException(nameof(timer));
        }

        /// <inheritdoc/>
        public void Init(EventHandler<NameAndTicksArguments> startHandler, EventHandler<NameArgument> stopHandler, EventHandler<NameAndTicksArguments> tickHandler)
        {
            this.timer.Started += startHandler;
            this.timer.Tick += tickHandler;
            this.timer.Stoped += stopHandler;
        }

        /// <inheritdoc/>
        public void Run()
        {
            this.timer.Run();
        }
    }
}

using System;
using CustomTimer.Factories;
using NUnit.Framework;
using CustomTimer.EventArguments;

#pragma warning disable CA1707

namespace CustomTimer.Tests
{
    public class CountDownNotifierTests
    {
        private CountDownNotifierFactory countDownNotifierFactory;
        private TimerFactory timerFactory;

        [SetUp]
        public void Setup()
        {
            this.countDownNotifierFactory = new CountDownNotifierFactory();
            this.timerFactory = new TimerFactory();
        }

        [TestCase("pie", 10)]
        [TestCase("cookies", 5)]
        [TestCase("pizza", 1)]
        public void Run_ValidTimer_AllEventsWorkAsExpected(string name, int totalTicks)
        {
            var timer = this.timerFactory.CreateTimer(name, totalTicks);
            var notifier = this.countDownNotifierFactory.CreateNotifierForTimer(timer);

            void TimerStarted(object sender, NameAndTicksArguments args)
            {
                Assert.AreEqual(name, args.Name);
                Assert.AreEqual(totalTicks, args.Ticks);
                Console.WriteLine($"Start timer '{args.Name}', total {args.Ticks} ticks");
            }

            void TimerStopped(object sender, NameArgument args)
            {
                Assert.AreEqual(name, args.Name);
                Console.WriteLine($"Stop timer '{args.Name}'");
            }

            var remainsTicks = totalTicks;

            void TimerTick(object sender, NameAndTicksArguments args)
            {
                remainsTicks -= 1;
                Assert.AreEqual(name, args.Name);
                Assert.AreEqual(remainsTicks, args.Ticks);
                Console.WriteLine($"Timer '{args.Name}', remains {args.Ticks} ticks");
            }

            notifier.Init(TimerStarted, TimerStopped, TimerTick);
            notifier.Run();

            Assert.AreEqual(0, remainsTicks);
        }

        [TestCase("pie", 10)]
        [TestCase("cookies", 5)]
        [TestCase("pizza", 1)]
        public void Run_NullDelegates_TimerIsWorking(string name, int totalTicks)
        {
            var timer = this.timerFactory.CreateTimer(name, totalTicks);
            var notifier = this.countDownNotifierFactory.CreateNotifierForTimer(timer);

            Assert.DoesNotThrow(() =>
            {
                notifier.Init(null, null, null);
                notifier.Run();
            });
        }

        [Test]
        public void Ctor_TimerIsNull_ThrowsArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => this.countDownNotifierFactory.CreateNotifierForTimer(null));
    }
}

﻿using System;
using CustomTimer.Implementation;
using Moq;
using NUnit.Framework;
using CustomTimer.EventArguments;

namespace CustomTimer.Tests
{
    public class TimerMockTests
    {
        [TestCase("alarm", 1)]
        [TestCase("alarm", 15)]
        public void EventMethodsCallCheck(string timerName, int ticks)
        {
            var timer = new Timer(timerName, ticks);
            var notifierMock = new Mock<CountDownNotifier>(timer);
            var notifier = notifierMock.Object;

            var onStartMock = new Mock<EventHandler<NameAndTicksArguments>>();
            var onStopMock = new Mock<EventHandler<NameArgument>>();
            var onTickMock = new Mock<EventHandler<NameAndTicksArguments>>();

            notifier.Init(onStartMock.Object, onStopMock.Object, onTickMock.Object);
            notifier.Run();

            onStartMock.Verify(n => n(It.IsAny<object>(), It.IsAny<NameAndTicksArguments>()), Times.Once);
            onStopMock.Verify(n => n(It.IsAny<object>(), It.IsAny<NameArgument>()), Times.Once);
            onTickMock.Verify(n => n(It.IsAny<object>(), It.IsAny<NameAndTicksArguments>()), Times.Exactly(ticks));
        }
    }
}
